prueba-routing

Para ejecutar el proyecto, primero se debe tener instalado ruby on rails,
La app se creaó con ruby  2.4.1 y rails 4.2.11.1 y SQLite3.

al momento de copiar la app en un computador local, debe ir a la carpeta principal
del proyecto y ejecutar el comando `bundle install`.

ejecutar el comando `rake db:setup` para crear la base de datos.

ejecutar el comando `rake db:migrate` para generar las tablas de la base de datos.

ejecutar el comando `rake db:seed` para generar datos de las ciudades.

1.- correr el servidor de la app prueba-routing con el comando `rails s`

2.- verificar que se levantó en el puerto 3000

ingresar a un navegador a localhost:3000

Se asume que:
 - los datos son ingresados correctamente, por lo que no se hizo validaciones



