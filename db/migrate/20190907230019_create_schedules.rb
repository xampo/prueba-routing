class CreateSchedules < ActiveRecord::Migration
  def change
    create_table :schedules do |t|
      t.references :vehicle, index: true, foreign_key: true
      t.references :driver, index: true, foreign_key: true
      t.references :route, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
