class CreateRoutes < ActiveRecord::Migration
  def change
    create_table :routes do |t|
      t.datetime :starts_at
      t.datetime :ends_at
      t.integer :load_type
      t.float :load_sum
      t.integer :stops_amount
      t.references :vehicle, index: true, foreign_key: true
      t.references :driver, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
