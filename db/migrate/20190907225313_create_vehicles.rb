class CreateVehicles < ActiveRecord::Migration
  def change
    create_table :vehicles do |t|
      t.float :capacity
      t.integer :load_type
      t.references :driver, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
