class CreateRouteAndCity < ActiveRecord::Migration
  def change
    create_table :cities_routes, id: false do |t|
      t.belongs_to :route, index: true, foreign_key: true
      t.belongs_to :city, index: true, foreign_key: true
    end
  end
end
