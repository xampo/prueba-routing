class CreateDriverCity < ActiveRecord::Migration
  def change
    create_table :cities_drivers, id: false do |t|
      t.belongs_to :driver, index: true, foreign_key: true
      t.belongs_to :city, index: true, foreign_key: true
    end
  end
end
