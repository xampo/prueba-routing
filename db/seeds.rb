# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

City.create([
  { name: 'Cerrillos' },
  { name: 'Cerro Navia' },
  { name: 'Conchalí' },
  { name: 'El Bosque' },
  { name: 'Estación Central' },
  { name: 'Huechuraba' },
  { name: 'Independencia' },
  { name: 'La Cisterna' },
  { name: 'La Florida' },
  { name: 'La Granja' },
  { name: 'La Pintana' },
  { name: 'La Reina' },
  { name: 'Las Condes' },
  { name: 'Lo Barnechea' },
  { name: 'Lo Espejo' },
  { name: 'Lo Prado' },
  { name: 'Macul' },
  { name: 'Maipú' },
  { name: 'Ñuñoa' },
  { name: 'Padre Hurtado' },
  { name: 'Pedro Aguirre Cerda' },
  { name: 'Peñalolén' },
  { name: 'Pirque' },
  { name: 'Providencia' },
  { name: 'Pudahuel' },
  { name: 'Puente Alto' },
  { name: 'Quilicura' },
  { name: 'Quinta Normal' },
  { name: 'Recoleta' },
  { name: 'Renca' },
  { name: 'San Bernardo' },
  { name: 'San Joaquín' },
  { name: 'San José de Maipo' },
  { name: 'San Miguel' },
  { name: 'San Ramón' },
  { name: 'Santiago ' }
])

Route.create([
  {starts_at: "2019-09-08 5:00:00", ends_at: "2019-09-08 9:00:00", load_type: 1, load_sum: 10.0, stops_amount: 1},
  {starts_at: "2019-09-08 7:00:00", ends_at: "2019-09-08 8:00:00", load_type: 2, load_sum: 11.0, stops_amount: 2},
  {starts_at: "2019-09-08 12:00:00", ends_at: "2019-09-08 15:00:00", load_type: 2, load_sum: 1.0, stops_amount: 4},
  {starts_at: "2019-09-08 14:00:00", ends_at: "2019-09-08 17:00:00", load_type: 1, load_sum: 13.0, stops_amount: 2},
  {starts_at: "2019-09-08 18:00:00", ends_at: "2019-09-08 22:00:00", load_type: 1, load_sum: 5.0, stops_amount: 3},
])

Driver.create([
  {name: "Driver2", phone: "56987654321", email: "email@email.com", max_stops: 6},
  {name: "Driver3", phone: "56987655321", email: "email@email.com", max_stops: 3},
  {name: "Driver4", phone: "56987657321", email: "email@email.com", max_stops: 4},
  {name: "Driver5", phone: "56987653321", email: "email@email.com", max_stops: 2}
])

Vehicle.create([
  {capacity: 50, load_type: 1 },
  {capacity: 20, load_type: 2 },
  {capacity: 13, load_type: 2 },
  {capacity: 22, load_type: 1 }
  ])
