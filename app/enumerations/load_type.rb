class LoadType < EnumerateIt::Base
  associate_values(general: 1, freezed: 2)
end
