module Drivers
  class ZoneFilter
    def initialize(drivers, zones_ids)
      self.drivers = drivers
      self.zones_ids = zones_ids
    end

    def filter_by_preferred_zones
      drivers.includes(:cities).select do |d|
        next if d.city_ids.empty?
        zones_ids.all? { |c_id| d.city_ids.include? (c_id) }
      end
    end

    private

    attr_accessor :drivers, :zones_ids
  end
end
