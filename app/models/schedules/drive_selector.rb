module Schedules
  class DriveSelector

    attr_accessor :route

    def initialize(route = nil)
      self.route = route
    end

    def compatible_drivers
      drivers = Driver.without_vehicle
      drivers_common_filter(drivers)
    end

    def compatible_vehicles
      vehicles = Vehicle.without_owner
      vehicles = vehicles.free_on(route.starts_at, route.ends_at)
      vehicles = vehicles.by_load_type(route.load_type)
      vehicles.by_capacity(route.load_sum)
    end

    def compatible_drivers_owners
      drivers = Driver.with_vehicle
      drivers = drivers_common_filter(drivers)
      owner_drivers_filter_vehicles_restrictions(drivers)
    end

    private

    def owner_drivers_filter_vehicles_restrictions(drivers)
      drivers.select do |driver|
        driver.vehicle.load_type == route.load_type &&
          driver.vehicle.capacity >= route.load_sum
      end
    end

    def drivers_common_filter(drivers)
      drivers = drivers_with_time_and_stops(drivers)
      drivers_by_zone(drivers)
    end

    def drivers_with_time_and_stops(drivers)
      drivers = drivers.free_on(route.starts_at, route.ends_at)
      drivers.by_max_stop(route.stops_amount)
    end

    def drivers_by_zone(drivers)
      by_zones = Drivers::ZoneFilter.new(drivers, route.city_ids)
      by_zones.filter_by_preferred_zones
    end


  end
end