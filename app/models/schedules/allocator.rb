module Schedules
  class Allocator
    def initialize
      @selector = Schedules::DriveSelector.new()
    end

    def allocate
      routes.each do |route|
        @selector.route = route

        if couple.size == 2
          create_schedule(route, couple[0], couple[1])
        else
          create_schedule(route, owner_couple[0], owner_couple[1]) if owner_couple.size == 2
        end
      end
    end

    private

    def schedule_params(route, driver, vehicle)
      {
        driver: driver,
        vehicle: vehicle,
        route: route
      }
    end

    def owner_couple
      driver = @selector.compatible_drivers_owners.first
      [
        driver,
        driver.try(:vehicle)
      ].compact
    end

    def couple
      [
        @selector.compatible_drivers.first,
        @selector.compatible_vehicles.first
      ].compact
    end

    def create_schedule(route, driver, vehicle)
      Schedule.create(schedule_params(route, driver, vehicle))
    end

    def routes
      Route.without_schedule
    end
  end
end
