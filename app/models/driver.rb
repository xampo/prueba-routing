class Driver < ActiveRecord::Base
  include ScheduleFilters

  has_one :vehicle
  has_many :schedules
  has_and_belongs_to_many :cities

  scope :by_max_stop, ->(max_stops) { where('max_stops >= ?', max_stops) }
  scope :without_vehicle, -> {where("NOT EXISTS #{vehicles_sql}")}
  scope :with_vehicle, -> {where("EXISTS #{vehicles_sql}")}

  accepts_nested_attributes_for :cities

  private

  def self.vehicles_sql
    '(SELECT 1 FROM vehicles WHERE vehicles.driver_id = drivers.id LIMIT 1)'
  end
end
