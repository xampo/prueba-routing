class Route < ActiveRecord::Base
  belongs_to :vehicle
  belongs_to :driver
  has_one :schedule
  has_and_belongs_to_many :cities

  scope :without_schedule, ->{joins('LEFT JOIN schedules ON schedules.route_id = routes.id').where('schedules.id IS NULL')}
  has_enumeration_for :load_type, with: ::LoadType, create_scopes: { prefix: true }, create_helpers: true

  accepts_nested_attributes_for :cities
end
