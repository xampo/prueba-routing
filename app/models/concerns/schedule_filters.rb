module ScheduleFilters
  extend ActiveSupport::Concern
  included do
    scope :free_on, ->(init_date, end_date) do
      joins("LEFT JOIN schedules
        ON schedules.#{schedules_foreign_key} = #{table_name}.id
        LEFT JOIN routes ON routes.id = schedules.route_id")
        .where('(NOT(:start <= routes.starts_at AND  :end >= routes.ends_at))
          AND (NOT(:start <= routes.starts_at AND  :end <= routes.ends_at))
          AND (NOT(:start >= routes.starts_at AND  :end >= routes.ends_at))
          AND (NOT(:start >= routes.starts_at AND  :end <= routes.ends_at))
          OR routes.starts_at IS NULL OR routes.ends_at  IS NULL',
          start: init_date, end: end_date)
    end

    def self.schedules_foreign_key
      reflections['schedules'].foreign_key
    end
  end
end
