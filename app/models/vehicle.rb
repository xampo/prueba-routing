class Vehicle < ActiveRecord::Base
  include ScheduleFilters

  belongs_to :driver
  has_many :schedules

  has_enumeration_for :load_type, with: ::LoadType, create_scopes: { prefix: true }, create_helpers: true
  scope :by_capacity, ->(capacity) { where('capacity >= ?', capacity) }
  scope :by_load_type, ->(load_type) { where(load_type: load_type) }
  scope :without_owner, -> { where(driver: nil) }
end
