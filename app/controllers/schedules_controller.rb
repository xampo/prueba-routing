class SchedulesController < ApplicationController
  def index
    allocator = Schedules::Allocator.new
    allocator.allocate
    @schedules = Schedule.all
  end
end
