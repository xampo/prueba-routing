json.extract! driver, :id, :name, :phone, :email, :max_stops, :created_at, :updated_at
json.url driver_url(driver, format: :json)
